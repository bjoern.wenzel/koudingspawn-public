package de.koudingspawn.servicebroker.config;

import org.springframework.cloud.servicebroker.model.Catalog;
import org.springframework.cloud.servicebroker.model.Plan;
import org.springframework.cloud.servicebroker.model.ServiceDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;

@Configuration
public class CatalogConfig {

    @Bean
    public Catalog catalog() {
        return new Catalog(getCatalog());
    }

    private List<ServiceDefinition> getCatalog() {

        return Collections.singletonList(new ServiceDefinition(
                "certificate-generator-service-broker",
                "tlscertificate",
                "Service to generate certificates from root certificates e.g. for Ingress",
                true,
                false,
                getPlans(),
                Arrays.asList("Certificate", "Ingress TLS"),
                getServiceDefinitionMetadata(),
                null,
                null
        ));

    }

    private List<Plan> getPlans() {
        return Collections.singletonList(new Plan("tls-certificate-one-year",
                "default",
                "generates a certificate for one year"
        ));
    }

    private Map<String, Object> getServiceDefinitionMetadata() {
        Map<String, Object> metadata = new HashMap<>();
        metadata.put("displayName", "TLS Certificate");
        metadata.put("longDescription", "Service to generate certificates from root certificates e.g. for Ingress");
        metadata.put("providerDisplayName", "de.koudingspawn");

        return metadata;
    }

}
