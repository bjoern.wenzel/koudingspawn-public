package de.koudingspawn.servicebroker.config;

import de.koudingspawn.servicebroker.business.X509CertificateGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

@Configuration
public class ServiceBrokerConfiguration {

    @Value("${broker.keystore.name}")
    private String keystoreName;

    @Value("${broker.keystore.password}")
    private String password;

    @Value("${broker.keystore.alias}")
    private String alias;

    @Bean
    public X509CertificateGenerator certificateGenerator() throws CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, SignatureException, NoSuchProviderException, InvalidKeyException, IOException {
        return new X509CertificateGenerator(keystoreName, password, alias);
    }

}
