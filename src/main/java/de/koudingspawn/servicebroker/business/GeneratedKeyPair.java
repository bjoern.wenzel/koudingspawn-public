package de.koudingspawn.servicebroker.business;

public class GeneratedKeyPair {
    private static String BEGINCERTIFICATE = "-----BEGIN CERTIFICATE-----\n";
    private static String ENDCERTIFICATE = "\n-----END CERTIFICATE-----\n";
    private static String BEGINKEY = "-----BEGIN RSA PRIVATE KEY-----\n";
    private static String ENDKEY = "\n-----END RSA PRIVATE KEY-----\n";

    private String privatekey;

    private String publickey;

    private String ca;

    public GeneratedKeyPair(String privatekey, String publickey, String ca) {
        this.privatekey = privatekey;
        this.publickey = publickey;
        this.ca = ca;
    }

    public GeneratedKeyPair() {
    }

    public void setPrivatekey(String privatekey) {
        if (!privatekey.contains(BEGINKEY)) {
            this.privatekey = BEGINKEY + privatekey + ENDKEY;
        } else {
            this.privatekey = privatekey;
        }

    }

    public void setPublickey(String publickey) {
        this.publickey = surroundWithCertHeaderAndFooter(publickey);
    }

    private String surroundWithCertHeaderAndFooter(String certificate) {
        if (!certificate.contains(BEGINCERTIFICATE)) {
            return BEGINCERTIFICATE + certificate + ENDCERTIFICATE;
        }

        return certificate;
    }

    public void setCA(String ca) {
        this.ca = surroundWithCertHeaderAndFooter(ca);
    }

    public String getPrivatekey() {
        return privatekey;
    }

    public String getPublickey() {
        return publickey;
    }

    public String getCa() {
        return ca;
    }
}
