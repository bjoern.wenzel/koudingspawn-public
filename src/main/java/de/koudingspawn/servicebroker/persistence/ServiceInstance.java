package de.koudingspawn.servicebroker.persistence;

import de.koudingspawn.servicebroker.business.GeneratedKeyPair;
import org.springframework.cloud.servicebroker.model.CreateServiceInstanceRequest;

import javax.persistence.*;

@Entity
public class ServiceInstance {

    @Id
    private String serviceInstanceId;

    private String cn;
    private int validity;
    @Column(columnDefinition = "TEXT")
    private String privatekey;
    @Column(columnDefinition = "TEXT")
    private String publickey;
    @Column(columnDefinition = "TEXT")
    private String ca;

    public ServiceInstance(CreateServiceInstanceRequest request) {
        this.serviceInstanceId = request.getServiceInstanceId();
        this.cn = request.getParameters().get("cn").toString();
        this.validity = Integer.parseInt(request.getParameters().get("validity").toString());
    }

    public ServiceInstance() {}

    public String getServiceInstanceId() {
        return serviceInstanceId;
    }

    public void setServiceInstanceId(String serviceInstanceId) {
        this.serviceInstanceId = serviceInstanceId;
    }

    public String getPrivatekey() {
        return privatekey;
    }

    public void setPrivatekey(String privatekey) {
        this.privatekey = privatekey;
    }

    public String getPublickey() {
        return publickey;
    }

    public void setPublickey(String publickey) {
        this.publickey = publickey;
    }

    public String getChain() {
        return this.publickey + "\n" + this.ca;
    }

    public String getCa() {
        return ca;
    }

    public void setGeneratedKeyPair(GeneratedKeyPair generatedKeyPair) {
        this.ca = generatedKeyPair.getCa();
        this.privatekey = generatedKeyPair.getPrivatekey();
        this.publickey = generatedKeyPair.getPublickey();
    }

    public void setCa(String ca) {
        this.ca = ca;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public int getValidity() {
        return validity;
    }

    public void setValidity(int validity) {
        this.validity = validity;
    }
}
