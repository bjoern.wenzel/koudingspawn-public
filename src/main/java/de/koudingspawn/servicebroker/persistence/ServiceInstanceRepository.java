package de.koudingspawn.servicebroker.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ServiceInstanceRepository extends JpaRepository<ServiceInstance, String> {

    Optional<ServiceInstance> findByServiceInstanceId(String serviceInstanceId);

}
