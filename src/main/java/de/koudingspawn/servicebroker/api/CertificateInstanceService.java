package de.koudingspawn.servicebroker.api;

import de.koudingspawn.servicebroker.business.GeneratedKeyPair;
import de.koudingspawn.servicebroker.business.X509CertificateGenerator;
import de.koudingspawn.servicebroker.persistence.ServiceInstance;
import de.koudingspawn.servicebroker.persistence.ServiceInstanceRepository;
import org.springframework.cloud.servicebroker.exception.ServiceBrokerException;
import org.springframework.cloud.servicebroker.exception.ServiceInstanceDoesNotExistException;
import org.springframework.cloud.servicebroker.exception.ServiceInstanceExistsException;
import org.springframework.cloud.servicebroker.model.*;
import org.springframework.cloud.servicebroker.service.ServiceInstanceService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CertificateInstanceService implements ServiceInstanceService {

    private final ServiceInstanceRepository serviceInstanceRepository;
    private final X509CertificateGenerator x509CertificateGenerator;

    public CertificateInstanceService(ServiceInstanceRepository serviceInstanceRepository, X509CertificateGenerator x509CertificateGenerator) {
        this.serviceInstanceRepository = serviceInstanceRepository;
        this.x509CertificateGenerator = x509CertificateGenerator;
    }

    @Override
    public CreateServiceInstanceResponse createServiceInstance(CreateServiceInstanceRequest request) {

        Optional<ServiceInstance> optInstance = this.serviceInstanceRepository.findByServiceInstanceId(request.getServiceInstanceId());
        if (optInstance.isPresent()) {
            throw new ServiceInstanceExistsException(request.getServiceInstanceId(), request.getServiceDefinitionId());
        }

        ServiceInstance serviceInstance = new ServiceInstance(request);
        Optional<GeneratedKeyPair> optKeyPair = generateKeyPair(serviceInstance.getCn(), serviceInstance.getValidity());
        if (!optKeyPair.isPresent()) {
            throw new ServiceBrokerException("Could not generate key pair");
        }
        serviceInstance.setGeneratedKeyPair(optKeyPair.get());
        serviceInstanceRepository.save(serviceInstance);

        return new CreateServiceInstanceResponse();
    }

    @Override
    public GetLastServiceOperationResponse getLastOperation(GetLastServiceOperationRequest request) {
        return new GetLastServiceOperationResponse().withOperationState(OperationState.SUCCEEDED);
    }

    @Override
    public DeleteServiceInstanceResponse deleteServiceInstance(DeleteServiceInstanceRequest request) {
        Optional<ServiceInstance> optInstance = serviceInstanceRepository.findByServiceInstanceId(request.getServiceInstanceId());

        if (!optInstance.isPresent()) {
            throw new ServiceInstanceDoesNotExistException(request.getServiceInstanceId());
        }

        serviceInstanceRepository.delete(request.getServiceInstanceId());

        return new DeleteServiceInstanceResponse();
    }

    @Override
    public UpdateServiceInstanceResponse updateServiceInstance(UpdateServiceInstanceRequest request) {
        Optional<ServiceInstance> optInstance = this.serviceInstanceRepository.findByServiceInstanceId(request.getServiceInstanceId());

        if (!optInstance.isPresent()) {
            throw new ServiceInstanceDoesNotExistException(request.getServiceInstanceId());
        }


        return null;
    }

    private Optional<GeneratedKeyPair> generateKeyPair(String cn, int validity) {
        try {
            GeneratedKeyPair certificate = x509CertificateGenerator.createCertificate(cn, validity);
            return Optional.of(certificate);
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
