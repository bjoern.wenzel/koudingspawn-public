package de.koudingspawn.servicebroker.api;

import de.koudingspawn.servicebroker.persistence.ServiceInstance;
import de.koudingspawn.servicebroker.persistence.ServiceInstanceRepository;
import org.springframework.cloud.servicebroker.exception.ServiceBrokerException;
import org.springframework.cloud.servicebroker.model.CreateServiceInstanceAppBindingResponse;
import org.springframework.cloud.servicebroker.model.CreateServiceInstanceBindingRequest;
import org.springframework.cloud.servicebroker.model.CreateServiceInstanceBindingResponse;
import org.springframework.cloud.servicebroker.model.DeleteServiceInstanceBindingRequest;
import org.springframework.cloud.servicebroker.service.ServiceInstanceBindingService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class CertificateServiceInstanceBindingService implements ServiceInstanceBindingService {

    private final ServiceInstanceRepository serviceInstanceRepository;

    public CertificateServiceInstanceBindingService(ServiceInstanceRepository serviceInstanceRepository) {
        this.serviceInstanceRepository = serviceInstanceRepository;
    }

    @Override
    public CreateServiceInstanceBindingResponse createServiceInstanceBinding(CreateServiceInstanceBindingRequest request) {
        Optional<ServiceInstance> optServiceInstance = serviceInstanceRepository.findByServiceInstanceId(request.getServiceInstanceId());

        if (!optServiceInstance.isPresent()) {
            throw new ServiceBrokerException("Not found service instance request");
        }

        ServiceInstance serviceInstance = optServiceInstance.get();

        Map<String, Object> certificateResponse = new HashMap<>();
        certificateResponse.put("tls.crt", serviceInstance.getChain());
        certificateResponse.put("tls.key", serviceInstance.getPrivatekey());

        return new CreateServiceInstanceAppBindingResponse().withCredentials(certificateResponse);
    }

    @Override
    public void deleteServiceInstanceBinding(DeleteServiceInstanceBindingRequest deleteServiceInstanceBindingRequest) {

    }
}
